Atmel - C drivers
=================

  driver development in C for Atmel microcontroller,
    using the MyAVR MK3 development board

Copyright (c) 2017 Martin Singer <martin.singer@web.de>


About
=====

Device drivers.
This file is part of myAVR-MK3_c-driver_libdev


Implemented driver
------------------

* KMX (key matrix)


Requirements
------------

### General

* avr-gcc
* avrdude


### Required libraries for tests and demos

The tests and demos are implemented for the myAVR-MK3 development board.
They use driver implemented in the 'myAVR-MK3_c-driver-libmk3' project.
(they are not required for compiling and installing this device drivers as library)

* `/usr/local/avr/lib/libmk3.a`
* `/usr/local/avr/include/mk3`


Documentation
-------------

### Creating the Doxygen reference

	$ doxygen doxygen.conf
	$ firefox doc/html/index.html


Usage
-----

Read and edit `src/Makefile.am`.

* `make all`        Make software.
* `make clean`      Clean out built project files.
* `make coff`       Convert ELF to AVR COFF
                      (for use with AVR Studio 3.x or VMLAB).
* `make extcoff`    Convert ELF to AVR Extended COFF
                      (for use with AVR Studio 4.07 or greater).
* `make program`    Download the hex file to the device, using avrdude.
                      Please customize the avrdude settings below first!
* `make filename.s` Just compile filename.c into the assembler code only.
* `make install`    Install drivers as library and headers on your system.
* `make uninstall`  Uninstall driver library and headers from your system.

To rebuild project do `make clean` then `make all`.


The Key Matirix (KMX) driver
============================

![Image: MyAVR MK3 development board](./img/photo_mk3.jpg "MyAVR MK3 development board")


This tests a key-matrix on PortE and PortF of the MC.

* PortE: output (driver / ROW)
* PortF: input  (reader / COL, has pull-up resistors enabled)

For the port definitions have a look at the "def.h" file.


Function Test
=============

The MC reader ports (COL) are configured to use the MC internal
pull-up resistors (no additional, external resistors are needed).
Because of this, the key matrix is low-active.

The driver ports (ROW) are set to HIGH by default.
While polling, the GND (LOW) becomes applied to one row,
set by the driver PORT (ROW).
The reader port (COL) reads the PINs and checks for the LOW signal,
by masking each PIN.
Because of the diodes the signal identifies a key definite.
Now the next ROW becomes LOW (the previous HIGH).
And so on.

For more information read the source or doxygen files.

Used files are:
* "kmx.c"
* "test.c"
* "lcd_out.c"


Circuit
-------

The arrow direction of the in/output pins defines the direction
of the data (not of voltage).

Schematic:

![Image: Schematic KMX](./img/schematic_kmx.png "Schematic KMX")


Photo:

![Image: Photo KMX](./img/photo_kmx.jpg "Photo KMX")

