/******************************************************************************
 Atmel - C driver - Key Matrix
   - Key Matrix (KMX) driver, implemented in C for Atmel microcontroller
 Copyright (c) 2016 - 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Key Matrix (KMX) device driver.
 *
 * @file      dev/kmx.c
 * @see       dev/kmx.h
 * @author    Martin Singer
 * @date      2016 - 2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 *
 *
 * The Scancode
 * ============
 *
 * - Bit[7]    := Condition (0 := COND_RELEASED, 1 := COND_PRESSED)
 * - Bit[6]    := Side      (0 := SIDE_LEFT,     1 := SIDE_RIGHT)
 * - Bit[5..3] := Row       (ROW    0 to 7)
 * - Bit[2..0] := Column    (COLUMN 0 to 7)
 *
 *	cond = 0..1
 *	side = 0..1
 *	row  = 0..7
 *	col  = 0..7
 *
 *	scancode = (cond<<7) | (side<<6) | (row<<3) | (col<<0);
 */


#include "kmx.h"

#include <avr/io.h>
#include <avr/interrupt.h>
//#include <avr/cpufunc.h>   // for _NOP()
//#include <stdlib.h>        // for NULL


/** Keyboard Side.
 *
 * Defines the side of a keyboard half.
 * This informatin is used for the scancode.
 *
 * @see kmx_get_scancode()
 * @defgroup KEYBOARD_SIDE
 * @{
 */
#define SIDE_LEFT  0 ///< Key is on the left  half.
#define SIDE_RIGHT 1 ///< Key is on the right half.
/** @} */

/** Key Condition States.
 *
 * @defgroup KEY_CONDITION
 * @{
 */
#define COND_RELEASED 0 ///< Key is in released condition.
#define COND_PRESSED  1 ///< Key is in  pressed condition.
/** @} */

/** Debounce Counter States.
 *
 * @defgroup DEBOUNCE_COUNTER
 * @{
 */
#define DEB_LOAD 0x05 ///< Set in debounce array for a key when its condition changed.
#define DEB_NULL 0x00 ///< Is this value reached, the debounce count down ends.
#define DEB_OFF  0xFF ///< Debounce count down is off for this key.
/** @} */

#define SIZE_FIFO 32 ///<  Size of Key Event FIFO Buffer (Key Roll Over).


volatile uint8_t Flag_Poll  = FLAG_NULL;
volatile uint8_t Flag_Event = FLAG_NULL;


/** Key Event FIFO Buffer struct.
 *
 * @struct Event_FIFO
 * @var    Event_FIFO::scancode[] Scancode array (store).
 * @var    Event_FIFO::pos_write Write position in array.
 * @var    Event_FIFO::pos_read Read position in array.
 *
 * - Array scancode[SIZE_FIFO] contains the scancodes.
 * - pos_write is the write position.
 * - pos_read  is the read  position.
 *
 * - pos_write points always to the new  write position.
 * - After a write action pos_wirte will be incremented.
 * - pos_read  points always to the next read  position.
 * - After a read  action pos_read  will be incremented.
 * - Is pos_read equal to pos_write, no read action will be done.
 */
volatile struct {
	uint8_t scancode[SIZE_FIFO];
	uint8_t pos_write;
	uint8_t pos_read;
} Event_Fifo = { { 0x00 }, 0, 0 }; ///< The Kye Event FIFO Buffer.


// Prototypes
void poll_keys(uint8_t);
void debounce_key(uint8_t, uint8_t, uint8_t, uint8_t);
void buffer_event(uint8_t, uint8_t, uint8_t, uint8_t);


/** ISR: Polling Timer.
 *
 * Timer0 is initialized to call this routine in 1KHz iterations.
 *
 * This routine sets the flag that indicates the system
 * it is time to poll the keys for detecting condition changes.
 */
ISR(TIMER0_COMPA_vect)
{
	volatile static uint8_t c = 0;

	cli();
	if (10 <= c++) {
//		_NOP();
		Flag_Poll = FLAG_SET;
		c = 0;
	}
	sei();
}


/*
 * Timer/Counter 0 Registers
 * =========================
 *
 * Source: ATmega2560 (2549L–AVR–08/07) Page 129, 170
 *
 * - TCCR0A - Timer/Counter 0: Control Register A
 *   - Bit 7 - COM0A1: Compare Match Output A Mode (r/w)
 *   - Bit 6 - COM0A0:                             (r/w)
 *   - Bit 5 - COM0B1: Compare Match Output B Mode (r/w)
 *   - Bit 4 - COM0B0:                             (r/w)
 *   - Bit 3 - reserved
 *   - Bit 2 - reserved
 *   - Bit 1 - WGM01:  Waveform Generation Mode    (r/w)
 *   - Bit 0 - WGM00:                              (r/w)
 *
 * - TCCR0B – Timer/Counter 0: Control Register B
 *   - Bit 7 – FOC0A: Force Output Compare A       (r/w)
 *   - Bit 6 – FOC0B: Force Output Compare B       (r/w)
 *   - Bit 5 – reserved                            (r)
 *   - Bit 4 – reserved                            (r)
 *   - Bit 3 – WGM02: Waveform Generation Mode     (r/w)
 *   - Bit 2 – CS02:  Clock Select                 (r/w)
 *   - Bit 1 – CS01:                               (r/w)
 *   - Bit 0 – CS00:                               (r/w)
 *
 * - TCNT0 – Timer/Counter 0: Register
 *   - TCNT0[7:0] (r/w)
 *
 * - OCR0A – Timer/Counter 0: Output Compare Register A
 *   - OCR0A[7:0] (r/w)
 *
 * - OCR0B – Timer/Counter 0: Output Compare Register B
 *   - OCR0B[7:0] (r/w)
 *
 * - TIMSK0 – Timer/Counter 0: Interrupt Mask Register
 *   - Bit 7 - reserved                                                       (r)
 *   - Bit 6 - reserved                                                       (r)
 *   - Bit 5 - reserved                                                       (r)
 *   - Bit 4 - reserved                                                       (r)
 *   - Bit 3 - reserved                                                       (r)
 *   - Bit 2 – OCIE0B: Timer/Counter0 Output Compare Match B Interrupt Enable (r/w)
 *   - Bit 1 – OCIE0A: Timer/Counter0 Output Compare Match A Interrupt Enable (r/w)
 *   - Bit 0 – TOIE0:  Timer/Counter0 Overflow Interrupt Enable               (r/w)
 *
 * - TIFR0 – Timer/Counter 0: Interrupt Flag Register
 *   - Bit 7 - reserved                                                       (r)
 *   - Bit 6 - reserved                                                       (r)
 *   - Bit 5 - reserved                                                       (r)
 *   - Bit 4 - reserved                                                       (r)
 *   - Bit 3 - reserved                                                       (r)
 *   - Bit 2 – OCF0B: Timer/Counter0 Output Compare B Match Flag              (r/w)
 *   - Bit 1 – OCF0A: Timer/Counter0 Output Compare A Match Flag              (r/w)
 *   - Bit 0 – TOV0:  Timer/Counter0 Overflow Flag                            (r/w)
 */
/*
 * Timer Modes
 * ===========
 *
 * Source: ATmega2560 (2549L–AVR–08/07) Page 129
 *
 *
 * Compare Output Mode, non PWM Mode
 * ---------------------------------
 *
 * COM0A1 | COM0A0 | Description
 * ------ | ------ | -----------
 * 0      | 0      | Normal port operation, OC0A disconnected.
 * 0      | 1      | Toggle OC0A on Compare Match
 * 1      | 0      | Clear  OC0A on Compare Match
 * 1      | 1      | Set    OC0A on Compare Match
 *
 *
 * Compare Output Mode, Fast PWM Mode
 * ----------------------------------
 *
 * COM0A1 | COM0A0 | Description
 * ------ | ------ | -----------
 * 0      | 0      | Normal port operation, OC0A disconnected.
 * 0      | 1      | WGM02 = 0: Normal Port Operation, OC0A Disconnected.
 *        |        | WGM02 = 1: Toggle OC0A on Compare Match.
 * 1      | 0      | Clear OC0A on Compare Match, set OC0A at BOTTOM,
 *        |        | (non-inverting mode).
 * 1      | 1      | Set OC0A on Compare Match, clear OC0A at BOTTOM,
 *        |        | (inverting mode).
 *
 *
 * Compare Output Mode, Phase Correct PWM Mode
 * -------------------------------------------
 *
 * COM0A1 | COM0A0 | Description
 * ------ | ------ | -----------
 * 0      | 0      | Normal port operation, OC0A disconnected.
 * 0      | 1      | WGM02 = 0: Normal Port Operation, OC0A Disconnected.
 *        |        | WGM02 = 1: Toggle OC0A on Compare Match.
 * 1      | 0      | Clear OC0A on Compare Match when up-counting. Set OC0A on
 *        |        | Compare Match when down-counting.
 * 1      | 1      | Set OC0A on Compare Match when up-counting. Clear OC0A on
 *        |        | Compare Match when down-counting.
 *
 *
 * Compare Output Mode, non-PWM Mode
 * ---------------------------------
 *
 * COM0B1 | COM0B0 | Description
 * ------ | ------ | -----------
 * 0      | 0      | Normal port operation, OC0B disconnected.
 * 0      | 1      | Toggle OC0B on Compare Match
 * 1      | 0      | Clear OC0B on Compare Match
 * 1      | 1      | Set OC0B on Compare Match
 *
 *
 * Compare Output Mode, Fast PWM Mode
 * ----------------------------------
 *
 * COM0B1 | COM0B0 | Description
 * ------ | ------ | -----------
 * 0      | 0      | Normal port operation, OC0B disconnected.
 * 0      | 1      | Reserved
 * 1      | 0      | Clear OC0B on Compare Match, set OC0B at BOTTOM,
 *        |        | (non-inverting mode).
 * 1      |        | 1 Set OC0B on Compare Match, clear OC0B at BOTTOM,
 *        |        | (inverting mode).
 *
 *
 * Compare Output Mode, Phase Correct PWM Mode
 * -------------------------------------------
 *
 * COM0B1 | COM0B0 | Description
 * ------ | ------ | -----------
 * 0      | 0      | Normal port operation, OC0B disconnected.
 * 0      | 1      | Reserved
 * 1      | 0      | Clear OC0B on Compare Match when up-counting. Set OC0B on
 *        |        | Compare Match when down-counting.
 * 1      | 1      | Set OC0B on Compare Match when up-counting. Clear OC0B on
 *        |        | Compare Match when down-counting.
 *
 *
 * Waveform Generation Mode Bit Description
 * ----------------------------------------
 *
 * Mode | WGM2 | WGM1 | WGM0 | Timer/Counter Mode | TOP  | Update of OCRx | TOV Flag Set
 * ---- | ---- | ---- | ---- | ------------------ | ---- | -------------- | ------------
 * 0    | 0    | 0    | 0    | Normal             | 0xFF | Immediate      | MAX
 * 1    | 0    | 0    | 1    | PWM, Phase Correct | 0xFF | TOP            | BOTTOM
 * 2    | 0    | 1    | 0    | CTC                | OCRA | Immediate      | MAX
 * 3    | 0    | 1    | 1    | Fast PWM           | 0xFF | TOP            | MAX
 * 4    | 1    | 0    | 0    | Reserved           | –    | –              | –
 * 5    | 1    | 0    | 1    | PWM, Phase Correct | OCRA | TOP            | BOTTOM
 * 6    | 1    | 1    | 0    | Reserved           | –    | –              | –
 * 7    | 1    | 1    | 1    | Fast PWM           | OCRA | BOTTOM         | TOP
 * Note:
 *   1. MAX    = 0xFF
 *   2. BOTTOM = 0x00
 *
 *
 * Clock Select Bit Description
 * ----------------------------
 *
 * CS02 | CS01 | CS00 | Description
 * 0    | 0    | 0    | No clock source (Timer/Counter stopped)
 * 0    | 0    | 1    | clk I/O /1    (No prescaling)
 * 0    | 1    | 0    | clk I/O /8    (From prescaler)
 * 0    | 1    | 1    | clk I/O /64   (From prescaler)
 * 1    | 0    | 0    | clk I/O /256  (From prescaler)
 * 1    | 0    | 1    | clk I/O /1024 (From prescaler)
 * 1    | 1    | 0    | External clock source on T0 pin. Clock on falling edge.
 * 1    | 1    | 1    | External clock source on T0 pin. Clock on rising  edge.
 */
/** Initialize the Key Matrix Driver.
 *
 * Howto
 * =====
 *
 * Init Row (ROW) as Driver
 * ------------------------
 *
 * - set DDR registers as output (DDR: DataDirectionRegister)
 * - set PORT registers to HIGH (initial)
 *
 *
 * Init Column (COL) as Reader
 * ---------------------------
 *
 * - set DDR registers as output
 * - set PORT pull-up resistors (low-active keys)
 * - the PIN registers are the input
 *
 *
 * Init TIMER0 (8 Bit Timer used to scale from 16Mhz to 1KHz)
 * ----------------------------------------------------------
 *
 * F_TIMER0 = F_CPU / (OCR0A * prescaler)
 * 1KHz = 16MHz / (250 * 64)
 *
 * - TCCR0A -> Set Mode 'Clear Timer on Compare Match' (CTC)
 * - TCCR0B -> Set 'Clocksource From Prescaler' to 64
 * - OCR0A  -> Set 'Output Compare Register A'  to 250
 * - TIMSK0 -> Enable 'Output Compare Match A'
 */
void kmx_init(void)
{
	// Flags
	Flag_Poll  = FLAG_NULL;
	Flag_Event = FLAG_NULL;

	// Ports
	KMX_DRIVER_DDR  = 0xFF; // direction output     (ROW)
	KMX_DRIVER_PORT = 0xFF; // set initial value

	KMX_READER_DDR  = 0x00; // direction input      (COL)
	KMX_READER_PORT = 0xFF; // set pull-up resistors

	// Timer
	TCCR0A = (1<<WGM01);
	TCCR0B = (1<<CS01) | (1<<CS00);
	OCR0A  = 250;
	TIMSK0 = (1<<OCIE0A);

	return;
}


/** Get next scancode from Key Event Buffer.
 *
 * @returns the next scancode from the Key Event Buffer.
 *
 * If the buffer read and write position is equivalent,
 * means there are no new events in the buffer.
 * If this condition happens, the Flag_Event becomes set to FLAG_NULL
 * which indicates this function sould not be called.
 * If it becomes called anyway, the function returns the default value 0x00
 * (which is a valid scancode).
 * There is no possiblity to signal the returned value is invalid!
 *
 * Is the Scancode read from the Buffer to a local varable,
 * the read position becomes incremented.
 * Is pos_read larger than SIZE_FIFO, pos_read is set to 0;
 *
 * Is pos_read equivalent to pos_write, all actual events are fetched.
 * The Flag_Events becomes set to FLAG_NULL,
 * which indicates, there are no more Key Events.
 */
uint8_t kmx_get_scancode(void)
{
	uint8_t sc = 0x00;

	if (Event_Fifo.pos_read != Event_Fifo.pos_write) {
		sc = Event_Fifo.scancode[Event_Fifo.pos_read];
		//TODO: delete flag which prevents from writing in the Event_Fifo
		if (SIZE_FIFO <= ++Event_Fifo.pos_read) {
			Event_Fifo.pos_read = 0;
		}
		if (Event_Fifo.pos_read == Event_Fifo.pos_write) {
			Flag_Event = FLAG_NULL; // no further events in the buffer
		}
	} else {
		/*
		 * Should not happen!
		 * Maybe try to set the Flag_Event to FLAG_NULL again.
		 */
		Flag_Event = FLAG_NULL;
	}
//	led_set_output(sc); //TEST
	return sc;
}


/** The Call function which calls all sub poll functions. */
void kmx_poll(void)
{
	poll_keys(SIDE_LEFT);
	poll_keys(SIDE_RIGHT);
	return;
}


/** The Poll function which reads every key for their event state.
 *
 * @param side 1st or 2nd KMX - SIDE_LEFT, SIDE_RIGHT
 *
 * The keys are low active.
 * The Functions pushes a 0 into the driver port (X)
 * and reads at the reader pins (Y) for the 0.
 * The cossing (XxY) key is pressed.
 * Every state becomes pushed to the debounce_key() function
 * to check up against previous states.
 * A change of the key state is a key event.
 * If a event has happen the debounce_key() function caches the event
 * to await the debouncing time and buffers the event in the Event_Fifo struct.
 */
void poll_keys(uint8_t side)
{
	uint8_t read = 0xFF;
	uint8_t row  = 0,
	        col  = 0,
	        cond = COND_RELEASED;

	for (row = 0; row < 8; row++) {
		for (col = 0; col < 8; col++) {
			switch (side) {
				case SIDE_LEFT:
					cli();
					KMX_DRIVER_PORT = ~(1<<row);
					read = ~KMX_READER_PIN & (1<<col);
					sei();
					break;
				case SIDE_RIGHT:
					; // not implemented yet
					break;
				default:
					; // do nothing
					break;
			}
			if (0x00 != read) {
				cond = COND_PRESSED;
			} else {
				cond = COND_RELEASED;
			}
			debounce_key(cond, side, row, col);
		}
	}
	return;
}


/** Checks and caches Key Events till the key is debounced.
 *
 * @param cond event condition - COND_RELEASED, COND_PRESSED
 * @param side 1st or 2nd KMX  - SIDE_LEFT, SIDE_RIGHT
 * @param row  row             - 0..7
 * @param col  column          - 0..7
 *
 * - This function gets called from the poll function for every single key.
 * - It checks if the debounce counter is active for a key first.
 * - Is the debounce counter DEB_OFF, the event slot for this key is free
 *   and the function checks whether the actual event differs from the cached.
 * - If it differes, the new event becomes copied to the cache slot
 *   and the debounce counter becomes set to the DEB_LOAD value for this key.
 * - Is the debounce counter not DEB_OFF, it is active
 *   and no new event becomes listet for this key, but the counter decremented.
 * - Has the debounce counter reached DEB_NULL the key should be debounced.
 *   The function checks if the actual event is already the same as that in the cache.
 *   That means the minimum time for an event
 *   and the specific time for the key to debounce has past.
 *   - If the actual event and the event in the cache is the same,
 *     the functions calls the buffer_event() function
 *     to create and buffer the event as scancode in the Buffer_Fifo struct.
 *     The function also sets the debounce counter to DEB_OFF
 *     for giving the key free for a new event.
 *   - If the actual event and the event in the cache is not the same,
 *     the function resets the condition in the cache
 *     and sets the debounce counter to DEB_OFF.
 *     It calls not the buffer_event() function. It's like nothing has happen.
 */
void debounce_key(uint8_t cond, uint8_t side, uint8_t row, uint8_t col)
{
	static uint8_t Table_Cond[2][8][8] = {{{ COND_RELEASED }}},
	               Table_Deb[2][8][8]  = {{{ DEB_OFF }}};

	if (DEB_OFF == Table_Deb[side][row][col]) {
		if (cond != Table_Cond[side][row][col]) {
			Table_Cond[side][row][col] = cond;
			Table_Deb[side][row][col]  = DEB_LOAD;
		}
	} else if (DEB_NULL == Table_Deb[side][row][col]) {
		if (cond == Table_Cond[side][row][col]) {
			buffer_event(cond, side, row, col);
//			lcd_put_char5x7('z'); //TEST
			Table_Deb[side][row][col] = DEB_OFF;
		} else {
			/*
			 * Error: debounce timed out
			 * reset condition; debounce counter off
			 */
			Table_Cond[side][row][col] = cond;
			Table_Deb[side][row][col]  = DEB_OFF;
		}
	} else {
		--Table_Deb[side][row][col];
	}
	return;
}


/** Create and buffer scancode
 *
 * @param cond event condition - COND_RELEASED, COND_PRESSED
 * @param side 1st or 2nd KMX  - SIDE_LEFT, SIDE_RIGHT
 * @param row  row             - 0..7
 * @param col  column          - 0..7
 *
 * Creates scancode from the different parameters
 * and bufferes it in the Event_Fifo struct.
 *
 * The write position points always to the next buffer slot.
 * After a write action the write position becomes incremented.
 * Is pos_write larger or equal to SIZE_FIFO it becomes set to 0.
 *
 * TODO: Is the pos_write equal to pos_read a flag beomes set
 * to prevent from writing into already not read event slots.
 * The flag becomes deleted by the read function.
 */
void buffer_event(uint8_t cond, uint8_t side, uint8_t row, uint8_t col)
{
	uint8_t sc = ((cond<<7) | (side<<6) | (row<<3) | (col<<0));
//	led_set_output(sc); //TEST

	// TODO: Check for FLAG buffer full here
	Event_Fifo.scancode[Event_Fifo.pos_write] = sc;
	Flag_Event = FLAG_SET;
	if (SIZE_FIFO <= ++Event_Fifo.pos_write) {
		Event_Fifo.pos_write = 0;
		if (Event_Fifo.pos_read == Event_Fifo.pos_write) {
			; // TODO: Set FLAG buffer full here
		}
	}
	return;
}

