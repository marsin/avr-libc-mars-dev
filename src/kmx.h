/******************************************************************************
 Atmel - C driver - Key Matrix
   - Key Matrix (KMX) driver, implemented in C for Atmel microcontroller
 Copyright (c) 2016 - 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Key Matrix (KMX) device driver.
 *
 * @file      dev/kmx.h
 * @see       dev/kmx.c
 * @author    Martin Singer
 * @date      2016 - 2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#ifndef DEVICE_KMX_H
#define DEVICE_KMX_H

#include <avr/io.h>


/** Key Matrix port definitions.
 *
 * @defgroup KMX_PORTS
 * @{
 */
#define KMX_DRIVER_DDR   DDRE  ///< direction output.
#define KMX_DRIVER_PORT  PORTE ///< driver / ROW.

#define KMX_READER_DDR   DDRF  ///< direction input.
#define KMX_READER_PORT  PORTF ///< pull-up resistors.
#define KMX_READER_PIN   PINF  ///< reader / COL.
/** @} */


/** Key Matrix Event Flags.
 *
 * Flag internal states for external functions.
 *
 * - If `Flag_Poll` is 'FLAG_SET', the internal timer has counted out.
 *   It's time to check for key events (poll keys to get their states).
 * - If `Flag_Event` is 'FLAG_SET', a key event (state change)
 *   has been detectet during polling the key states.
 *
 * @defgroup KMX_EVENT_FLAGS
 * @{
 */
#define FLAG_SET  1 ///< A event happened.
#define FLAG_NULL 0 ///< No event happened (reset state).

extern volatile uint8_t Flag_Poll;  ///< Signals it's time to poll the keys.
extern volatile uint8_t Flag_Event; ///< Signals an key event has happened.
/** @} */


extern void kmx_init(void);
extern void kmx_poll(void);
extern uint8_t kmx_get_scancode(void);


#endif // DEVICE_KMX_H

