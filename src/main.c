/******************************************************************************
 Atmel - C driver
   - driver development in C for Atmel microcontroller,
     using the MyAVR MK3 development board
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Main program.
 *
 * @file      main.c
 * @date      2017
 * @author    Martin Singer
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */

#ifndef F_CPU
  #define F_CPU 16000000ul
#endif

#include "test.h"


/** Main function. */
int main(void)
{
	test_dev_init();
	test_dev_kmx();

	while (1) {
		;
	}

	return 0;
}

