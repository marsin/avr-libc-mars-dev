/******************************************************************************
 Atmel - C driver - hardware devices - tests
   - myAVR MK3 board driver, implemented in C for Atmel microcontroller
 Copyright (c) 2016 - 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Periphery hardware device driver tests.
 *
 * @file      test_dev.c
 * @see       test_dev.h
 * @author    Martin Singer
 * @date      2016 - 2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#include <avr/interrupt.h>
#include <mk3/board.h>

#include "test.h"


/** Init device driver */
void test_dev_init(void)
{
	kmx_init();
}


/** TEST Function: Key Matrix (extern circuit).
 *
 * Shows the key press events on LCD
 */
void test_dev_kmx(void)
{
	uint8_t sc = 0x00;
	char    c  = 0x00;

	sei(); // interrupt on
	while (1) {
		switch (Flag_Poll) {
			case FLAG_SET:
				kmx_poll();
				break;
		}
		switch (Flag_Event) {
			case FLAG_SET:
				sc = kmx_get_scancode();
				if (0x80 == (sc & 0x80)) {
					c = sc2ascii(sc);
					lcd_putchar(c);
				}
				break;
			default:
				// do nothing
				break;
		}
	}
	cli();
}

