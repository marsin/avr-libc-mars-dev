/******************************************************************************
 Atmel - C driver - hardware devices - tests
   - myAVR MK3 board driver, implemented in C for Atmel microcontroller
 Copyright (c) 2016 - 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Periphery hardware device driver tests.
 *
 * @file      test_dev.h
 * @see       test_dev.c
 * @author    Martin Singer
 * @date      2016 - 2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#ifndef TEST_DEV_H
#define TEST_DEV_H

#include "kmx.h"


void test_dev_init(void);
void test_dev_kmx(void);


#endif // TEST_DEV_H

